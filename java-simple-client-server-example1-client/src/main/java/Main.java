import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        /*
        * 2 parameters: the address of the host we wanna connect to and the port number
        * since we wanna connect to a server running in the same host (your computer in this case)
        * we use "localhost" for the host value. 127.0.0.1 would also work.
        * we need to specify the same port number as the server is listening to.
        * */
        try(Socket socket = new Socket("localhost", 5000)){
            BufferedReader clientReader = new BufferedReader(new InputStreamReader(socket.getInputStream()));
            PrintWriter clientWriter = new PrintWriter(socket.getOutputStream(), true);
            /*
            * we create a scanner to read the input from the console
            * */
            Scanner scanner = new Scanner(System.in);
            String echoString;
            String response;

            do{
                System.out.println("Input string to be sent to the server:");
                echoString = scanner.nextLine();
                clientWriter.println(echoString);
                if(!echoString.equals("quit")){
                    response = clientReader.readLine();
                    System.out.println(response);
                }
            }while(!echoString.equals("quit"));

        }
        catch(IOException e){
            System.out.println("Client error: " + e.getMessage());
        }
    }
}
